package uz.myapps.learnjava.signinfragments

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_sign_up.view.*
import uz.myapps.learnjava.MainActivity
import uz.myapps.learnjava.R
import uz.myapps.learnjava.SplashActivity


class SignUpFragment : Fragment() {
    lateinit var mAuth: FirebaseAuth
    lateinit var db: FirebaseFirestore
    lateinit var email: String
    lateinit var password: String
    lateinit var user_name: String
    val RC_SIGN_IN: Int = 1
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleSignInOptions: GoogleSignInOptions
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_sign_up, container, false)
        mAuth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()
        root.btn_signup.setOnClickListener {
            email = root.email_signup.text.toString()
            password = root.password_signup.text.toString()
            user_name = root.name_signup.text.toString()
            if (email == "" || password == "" || user_name == "") {
                Toast.makeText(
                    requireContext(),
                    "Iltimos, ma'lumotlarni to'liq kiriting",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (password.length<6 ) {
                Toast.makeText(
                    requireContext(),
                    "Parol 6 tadan kam bo'lmagan belgidan iborat bo'lishi kerak",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                mAuth.createUserWithEmailAndPassword(
                    email,
                    password
                ).addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        val user = mAuth.currentUser
                        updateUI(user_name, user!!, password)
                        val signInFragment = SignInFragment()
                        val bundle = Bundle()
                        bundle.putString("email", email)
                        bundle.putString("password", password)
                        signInFragment.arguments = bundle
                        fragmentManager?.beginTransaction()?.replace(R.id.fragment, signInFragment)
                            ?.commit()
                    }
                }
            }
        }
        root.cvgoogle.setOnClickListener {
            if (isNetworkAvailable()) {
                configureGoogleSignIn()
                signIn()
            } else {
                val builder = AlertDialog.Builder(requireContext())
                builder.setMessage("Internet ulanmagan!")
                    .setCancelable(false)
                    .setPositiveButton(
                        "Ulash"
                    ) { p0, p1 -> startActivity(Intent(Settings.ACTION_WIFI_SETTINGS)) }
                    .setNegativeButton("Bekor qilish") { _, _ ->
                        requireActivity().onBackPressed()
                    }
                builder.show()
            }
        }
        return root
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager: ConnectivityManager =
            requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val wificonn: NetworkInfo =
            connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)!!
        val mobileconn: NetworkInfo =
            connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)!!
        return wificonn != null && wificonn.isConnected || mobileconn != null && mobileconn.isConnected
    }

    private fun updateUI(name: String, firebaseUser: FirebaseUser, password: String) {
        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(firebaseUser!!.uid)
            .get()
            .addOnSuccessListener { p0 ->
                if (p0.exists()) {
                    if (password == "") {
                        startActivity(Intent(requireContext(), MainActivity::class.java))
                        requireActivity().finish()
                    } else {
                        val signInFragment = SignInFragment()
                        val bundle = Bundle()
                        bundle.putString("email", email)
                        bundle.putString("password", password)
                        signInFragment.arguments = bundle
                        fragmentManager?.beginTransaction()?.replace(R.id.fragment, signInFragment)
                            ?.commit()
                    }
                } else {
                    val user: MutableMap<String, Any> = HashMap()
                    user["name"] = name
                    user["email"] = firebaseUser.email.toString()
                    user["password"] = password
                    user["score"] = 0
                    user["img"] = firebaseUser.photoUrl.toString()
                    user["haveFinished"] = false
                    db.collection("users").document(firebaseUser!!.uid)
                        .set(user)
                        .addOnSuccessListener {
                        }
                        .addOnFailureListener(OnFailureListener { e ->
                            Log.w(
                                "SignUp",
                                "Error adding document",
                                e
                            )
                        })
                }
            }.addOnFailureListener {
            }


    }

    private fun signIn() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), mGoogleSignInOptions)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    firebaseAuthWithGoogle(account)
                }
            } catch (e: ApiException) {
                Log.d("login", e.toString())
                Toast.makeText(requireContext(), "Google sign in failed:(", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount?) {
        val credential = GoogleAuthProvider.getCredential(account!!.idToken, null)
        mAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                var firebaseUser = mAuth.currentUser
                updateUI(firebaseUser?.displayName!!, firebaseUser, "")
                startActivity(Intent(requireContext(), MainActivity::class.java))
                requireActivity().finish()
            } else {
                Toast.makeText(requireContext(), "Google sign in failed:(", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }
}