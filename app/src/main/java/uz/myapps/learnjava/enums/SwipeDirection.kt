package uz.myapps.learnjava.enums

enum class SwipeDirection {
    ALL, LEFT, RIGHT, NONE ;
}
