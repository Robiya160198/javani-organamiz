package uz.myapps.learnjava.repository

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import uz.myapps.learnjava.models.*

class JavaRepository {

    fun getBasicPlanList(): MutableLiveData<List<BasicPlan>> {
        var list = MutableLiveData<List<BasicPlan>>()
        var tempList = ArrayList<BasicPlan>()
        val db = FirebaseFirestore.getInstance()
        db.collection("Basic Plans")
            .get()
            .addOnSuccessListener { p0 ->
                if (!p0!!.isEmpty) {
                    val documents = p0.documents
                    for (i in documents) {
                        val title = i.toObject(BasicPlan::class.java)
                        tempList.add(title!!)
                    }
                    list.value = tempList
                }
            }.addOnFailureListener {
                Log.d(TAG, "getBasicPlanList: $it")
            }
        return list
    }
    fun getPlanListByTitleId(basicPlanId: String): MutableLiveData<List<Plan>> {
        var list = MutableLiveData<List<Plan>>()
        var tempList = ArrayList<Plan>()
        val db = FirebaseFirestore.getInstance()
        db.collection("Plans").whereEqualTo("titleId",basicPlanId).orderBy("id")
            .get()
            .addOnSuccessListener { p0 ->
                if (!p0!!.isEmpty) {
                    val documents = p0.documents
                    for (i in documents) {
                        val title = i.toObject(Plan::class.java)
                        tempList.add(title!!)
                    }
                    list.value = tempList
                }
            }.addOnFailureListener {
                Log.d(TAG, "getPlanList: $it")
            }
        return list
    }
    fun getPlanList(): MutableLiveData<List<Plan>> {
        var list = MutableLiveData<List<Plan>>()
        var tempList = ArrayList<Plan>()
        val db = FirebaseFirestore.getInstance()
        db.collection("Plans")
            .get()
            .addOnSuccessListener { p0 ->
                if (!p0!!.isEmpty) {
                    val documents = p0.documents
                    for (i in documents) {
                        val title = i.toObject(Plan::class.java)
                        tempList.add(title!!)
                    }
                    list.value = tempList
                }
            }.addOnFailureListener {
                Log.d(TAG, "getPlanList: $it")
            }
        return list
    }

    fun getLessonList(): MutableLiveData<List<Lesson>> {
        var list = MutableLiveData<List<Lesson>>()
        var tempList = ArrayList<Lesson>()
        val db = FirebaseFirestore.getInstance()
        db.collection("Lessons")
            .get()
            .addOnSuccessListener { p0 ->
                if (!p0!!.isEmpty) {
                    val documents = p0.documents
                    for (i in documents) {
                        val title = i.toObject(Lesson::class.java)
                        tempList.add(title!!)
                    }
                    list.value = tempList
                }
            }.addOnFailureListener {
                Log.d(TAG, "getLessonList: $it")
            }

        return list
    }
    fun getTestList(): MutableLiveData<List<Test>> {
        var list = MutableLiveData<List<Test>>()
        var tempList = ArrayList<Test>()
        val db = FirebaseFirestore.getInstance()
        db.collection("Tests")
            .get()
            .addOnSuccessListener { p0 ->
                if (!p0!!.isEmpty) {
                    val documents = p0.documents
                    for (i in documents) {
                        val title = i.toObject(Test::class.java)
                        tempList.add(title!!)
                    }
                    list.value = tempList
                }
            }.addOnFailureListener {
                Log.d(TAG, "getTestList: $it")
            }

        return list
    }
    fun updateUsers(
        titleId: String,
        id: String,
        currentUser: FirebaseUser,
        isEnd: Boolean
    ):MutableLiveData<List<User>>{
        val docRef =
            FirebaseFirestore.getInstance().collection("Plans")
                .document(id)
        docRef.update(
            "haveUsedUsers",
            FieldValue.arrayUnion(currentUser.uid)
        )
        if(!isEnd) {
            val docRef1 =
                FirebaseFirestore.getInstance().collection("Plans")
                    .document((id.toInt() + 1).toString())
            docRef1.update(
                "areUsingUsers",
                FieldValue.arrayUnion(currentUser.uid)
            )
        }
        var list = MutableLiveData<List<User>>()
        var tempList = ArrayList<User>()
        val email = currentUser.email!!
        val password = ""
        val name = currentUser.displayName!!
        val score = 0
        tempList.add(User(email,name,password,score,""))
        list.value=tempList
        return list
    }

    fun getUser(currentUser: FirebaseUser): MutableLiveData<User> {
        var user = MutableLiveData<User>()
        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(currentUser.uid)
            .get()
            .addOnSuccessListener { p0 ->
                val data = p0.data
                user.value=User(data!!["email"].toString(),data["name"].toString(),data["password"].toString(),data["score"].toString().toInt(),data["img"].toString())
            }.addOnFailureListener {
                Log.d(TAG, "getUserList: $it")
            }
        return user
    }
    fun getUsers(): MutableLiveData<List<User>> {
        var list = MutableLiveData<List<User>>()
        var tempList = ArrayList<User>()
        val db = FirebaseFirestore.getInstance()
        db.collection("users").orderBy("score",Query.Direction.DESCENDING)
            .get()
            .addOnSuccessListener { p0 ->
                if (!p0!!.isEmpty) {
                    val documents = p0.documents
                    for (i in documents) {
                        val title = i.toObject(User::class.java)
                        tempList.add(title!!)
                    }
                    list.value = tempList
                }
            }.addOnFailureListener {
                Log.d(TAG, "getUserList: $it")
            }
        return list
    }
}