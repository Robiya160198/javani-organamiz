package uz.myapps.learnjava

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    companion object {
        lateinit var bottom_view: BottomNavigationView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sharedPreferences =
            this.getSharedPreferences("DarkMode", Context.MODE_PRIVATE)
        if (sharedPreferences.getInt("isDark", 0) == 1) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            rlMain.setBackgroundColor(Color.parseColor("#202020"))
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            rlMain.setBackgroundResource(R.drawable.ic_back_mainn)
        }
        bottom_view = findViewById(R.id.bottom_navview)
        bottom_view.setupWithNavController(fragment_container.findNavController())
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}