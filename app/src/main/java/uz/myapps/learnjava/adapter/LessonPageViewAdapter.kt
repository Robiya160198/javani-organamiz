package uz.myapps.learnjava.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import uz.myapps.learnjava.fragments.LectureFragment
import uz.myapps.learnjava.fragments.TestFragment
import uz.myapps.learnjava.models.BasicPlan
import uz.myapps.learnjava.models.Lesson
import uz.myapps.learnjava.models.Plan
import uz.myapps.learnjava.models.Test

class LessonPageViewAdapter(
    var lectureList: List<Lesson>,
    var testList: List<Test>,
    var plan: Plan,
    var isEnd: Boolean,
    var basicPlan: BasicPlan,
    fm: FragmentManager
) :
    FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position%2){
            0 -> {
                LectureFragment.newInstance(lectureList[position/2])
            }
            else -> {
                TestFragment.newInstance(testList[position/2],testList.size,plan,isEnd,basicPlan)
            }
        }
    }

    override fun getCount(): Int = lectureList.size + testList.size

}