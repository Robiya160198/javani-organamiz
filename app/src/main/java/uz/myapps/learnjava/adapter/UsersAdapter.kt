package uz.myapps.learnjava.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_rv_user.view.*
import uz.myapps.learnjava.R
import uz.myapps.learnjava.models.User

class UsersAdapter(var list: List<User>):RecyclerView.Adapter<UsersAdapter.VH>(){
    inner class VH(itemView: View):RecyclerView.ViewHolder(itemView){
        fun onBind(user: User){
            Picasso.get().load(user.img).into(itemView.img_profile)
            itemView.name_profile.text = user.name
            itemView.email_profile.text = user.email
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val inflate =
            LayoutInflater.from(parent?.context).inflate(R.layout.item_rv_user, parent, false)
        return VH(inflate)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBind(list[position])
    }
}