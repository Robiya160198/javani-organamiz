package uz.myapps.learnjava.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import uz.myapps.learnjava.fragments.LectureFragment
import uz.myapps.learnjava.fragments.TestFragment
import uz.myapps.learnjava.models.BasicPlan
import uz.myapps.learnjava.models.Lesson
import uz.myapps.learnjava.models.Plan
import uz.myapps.learnjava.models.Test

class QuizAdapter(
    var testList: List<Test>,
    var plan: Plan,
    var basicPlan:BasicPlan,
    fm: FragmentManager
) :
    FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment =
        TestFragment.newInstance(testList[position], testList.size, plan, true,basicPlan)

    override fun getCount(): Int = testList.size
}


