package uz.myapps.learnjava.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.item_rv_courses.view.*
import uz.myapps.learnjava.R
import uz.myapps.learnjava.models.Plan

class MenuAdapter(
    var list: List<Plan>,
    var currentUser: FirebaseUser,
    var click: onClick
) :
    RecyclerView.Adapter<MenuAdapter.VH>() {
    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun onBind(plan: Plan, position: Int) {
            var t=false
            var isEnd=false
            if(itemCount==position+1){
                isEnd=true
            }
            itemView.name.text = plan.name
            if (plan.haveUsedUsers.contains(currentUser.uid)) {
                t=true
                itemView.setBackgroundResource(R.drawable.ic_redorange_huge_radius)
                itemView.locked.setImageResource(R.drawable.ic_baseline_check_24)
            } else
                if (plan.id.toString() == "1" || plan.areUsingUsers.contains(currentUser.uid)) {
                    t=true
                    itemView.setBackgroundResource(R.drawable.ic_greenwhite)
                    itemView.locked.setImageResource(R.drawable.ic_chevron_right_black_24dp)
                }else{
                    itemView.setBackgroundResource(R.drawable.ic_locked_back)
                    itemView.locked.setImageResource(R.drawable.ic_baseline_lock_24)
                }
            itemView.setOnClickListener {
                if(t)
                click.onclick(plan,isEnd)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val inflate =
            LayoutInflater.from(parent?.context).inflate(R.layout.item_rv_courses, parent, false)
        return VH(inflate)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBind(list[position],position)
    }

    interface onClick {
        fun onclick(plan: Plan, isEnd: Boolean)
    }
}