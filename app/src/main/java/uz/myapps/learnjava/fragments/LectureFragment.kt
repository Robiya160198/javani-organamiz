package uz.myapps.learnjava.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_lecture.view.*
import uz.myapps.learnjava.R
import uz.myapps.learnjava.models.Lesson


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"

class LectureFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: Lesson? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getSerializable(ARG_PARAM1) as Lesson
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_lecture, container, false)
        lectureName = inflate.findViewById(R.id.name_lecture)
        part1 = inflate.findViewById(R.id.part1)
        code1 = inflate.findViewById(R.id.code1)
        part2 = inflate.findViewById(R.id.part2)
        code2 = inflate.findViewById(R.id.code2)
        note = inflate.findViewById(R.id.note)
        lectureName.text = param1?.name
        if (param1?.part1 != "")
            part1.text = param1?.part1?.replace("&n&", "\n", false)
        else
            part1.visibility = View.GONE
        if (param1?.part2 != "")
            part2.text = param1?.part2?.replace("&n&", "\n", false)
        else
            part2.visibility = View.GONE
        if (param1?.code1 != "")
            code1.text = param1?.code1?.replace("&n&", "\n", false)
        else
            code1.visibility = View.GONE
        if (param1?.code2 != "") {
            code2.text = param1?.code2?.replace("&n&", "\n", false)!!
        } else
            code2.visibility = View.GONE
        if (param1?.note != "")
            note.text = param1?.note?.replace("&n&", "\n", false)
        else
            inflate.note_layout.visibility = View.GONE

        inflate.btn_next.setOnClickListener {
            LessonFragment.vp.currentItem = LessonFragment.vp.currentItem + 1
        }
        return inflate
    }

    companion object {
        lateinit var lectureName: TextView
        lateinit var code1: TextView
        lateinit var code2: TextView
        lateinit var part1: TextView
        lateinit var part2: TextView
        lateinit var note: TextView

        @JvmStatic
        fun newInstance(param1: Lesson) =
            LectureFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM1, param1)
                }
            }
    }
}