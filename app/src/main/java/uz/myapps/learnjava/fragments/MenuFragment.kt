package uz.myapps.learnjava.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_menu.view.*
import uz.myapps.learnjava.R
import uz.myapps.learnjava.adapter.MenuAdapter
import uz.myapps.learnjava.models.BasicPlan
import uz.myapps.learnjava.models.Plan
import uz.myapps.learnjava.viewmodels.JavaViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MenuFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MenuFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_menu, container, false)
        val basicPlan = requireArguments().getSerializable("basicPlan")!! as BasicPlan
        val currentUser = FirebaseAuth.getInstance().currentUser!!
        val javaViewModel = ViewModelProviders.of(this)[JavaViewModel::class.java]
        javaViewModel.getPlanDataByTitleId(basicPlan?.id).observe(requireActivity(), Observer {
            var adapter =
                uz.myapps.learnjava.adapter.MenuAdapter(
                    it,
                    currentUser,
                    object : MenuAdapter.onClick {
                        override fun onclick(plan: Plan, isEnd: Boolean) {
                            val bundleOf = bundleOf("plan" to plan, "isEnd" to isEnd,"basicPlan" to basicPlan)
                            val lessonfragment = LessonFragment()
                            lessonfragment.arguments = bundleOf
                            fragmentManager?.beginTransaction()
                                ?.addToBackStack("menu")?.replace(R.id.container, lessonfragment)
                                ?.commit()
                        }
                    })
            inflate.rv_course.adapter = adapter
            inflate.quiz.visibility=View.VISIBLE

        })
        inflate.quiz.setOnClickListener {
            val bundleOf = bundleOf("plan" to "","basicPlan" to basicPlan)
            val lessonfragment = LessonFragment()
            lessonfragment.arguments = bundleOf
            fragmentManager?.beginTransaction()
                ?.addToBackStack("menu")?.replace(R.id.container, lessonfragment)
                ?.commit()
        }
        inflate.back_menu.setOnClickListener {
            requireActivity().onBackPressed()
        }
        return inflate
    }

    companion object {
        var count = 0

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MenuFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}