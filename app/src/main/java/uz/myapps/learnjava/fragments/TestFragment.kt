package uz.myapps.learnjava.fragments

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_test.*
import kotlinx.android.synthetic.main.fragment_test.view.*
import kotlinx.android.synthetic.main.fragment_title.view.*
import uz.myapps.learnjava.MainActivity
import uz.myapps.learnjava.R
import uz.myapps.learnjava.enums.SwipeDirection
import uz.myapps.learnjava.models.BasicPlan
import uz.myapps.learnjava.models.Plan
import uz.myapps.learnjava.models.Test
import uz.myapps.learnjava.viewmodels.JavaViewModel

private const val ARG_PARAM1 = "test"
private const val ARG_PARAM2 = "listSize"
private const val ARG_PARAM3 = "plan"
private const val ISEND = "isEnd"
private const val BASICPLAN = "basicPlan"

class TestFragment : Fragment() {
    private var test: Test? = null
    private var listSize: Int? = null
    private var plan: Plan? = null
    private var isEnd: Boolean? = null
    private var basicPlan: BasicPlan? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            test = it.getSerializable(ARG_PARAM1) as Test
            listSize = it.getInt(ARG_PARAM2)
            plan = it.getSerializable(ARG_PARAM3) as Plan
            isEnd = it.getBoolean(ISEND)
            basicPlan = it.getSerializable(BASICPLAN) as BasicPlan
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_test, container, false)
        val currentUser = FirebaseAuth.getInstance().currentUser!!
        val javaViewModel = ViewModelProviders.of(this)[JavaViewModel::class.java]
        if (plan?.id!= 0 && !plan!!.haveUsedUsers.contains(currentUser.uid))
            LessonFragment.vp.setAllowedSwipeDirection(SwipeDirection.LEFT)
        if (LessonFragment.vp.currentItem == 0) MenuFragment.count = 0
        var isCheck = 0
        var list = ArrayList<String>()
        list.add(test?.answer1!!)
        list.add(test?.answer2!!)
        list.add(test?.answer3!!)
        list.add(test?.answer4!!)
        list.shuffle()
        root.question.text = test?.question
        root.answer1.text = list[0]
        root.answer2.text = list[1]
        root.answer3.text = list[2]
        root.answer4.text = list[3]
        var answer = ""
        root.rg.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.answer1 -> {
                    answer = answer1.text.toString()
                }
                R.id.answer2 -> {
                    answer = answer2.text.toString()
                }
                R.id.answer3 -> {
                    answer = answer3.text.toString()
                }
                R.id.answer4 -> {
                    answer = answer4.text.toString()
                }
            }
        }
        root.btn_check.setOnClickListener {
            when (isCheck) {
                0 -> {
                    if (answer == test?.realAnswer) {
                        root.btn_check.text = "Davom etish"
                        Snackbar.make(it, "To'g'ri", Snackbar.LENGTH_LONG)
                            .setTextColor(resources.getColor(R.color.white))
                            .setAction("Action", null).show()
                        LessonFragment.vp.setAllowedSwipeDirection(SwipeDirection.ALL)
                        MenuFragment.count++
                        if (plan!!.haveUsedUsers.contains(currentUser.uid)||basicPlan!!.haveUsedUsers.contains(currentUser.uid)) {
                            javaViewModel.getTestData().observe(requireActivity(), Observer {
                                if (LessonFragment.vp.currentItem != (listSize!! * 2 - 1)) {
                                    LessonFragment.vp.currentItem += 1
                                } else {
                                    requireActivity().onBackPressed()
                                }
                            })
                        } else
                            if (MenuFragment.count == listSize) {
                                if(plan!!.id==0){
                                    if (basicPlan!!.id == "6")
                                        isCheck = 5
                                    else{
                                        nextPlanData(currentUser)
                                        startActivity(Intent(requireContext(),MainActivity::class.java))
                                }}
                                else{
                                updateUserScore(currentUser, listSize!!)
                                javaViewModel.updateUsers(
                                    plan!!.titleId,
                                    plan!!.id.toString(),
                                    currentUser,
                                    isEnd!!
                                )
                                    .observe(requireActivity(), Observer {
                                        if (plan!!.titleId == "6")
                                            isCheck = 5
                                    })
                            }} else
                                if (LessonFragment.vp.currentItem != (listSize!! * 2 - 1)) {
                                    isCheck = 1
                                } else {
                                    requireActivity().onBackPressed()
                                }

                    } else {
                        isCheck = 3
                        for (i in 0 until 4) {
                            root.rg.getChildAt(i).isEnabled = false
                        }
                        Snackbar.make(it, "Noto'g'ri", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                        root.btn_check.text = "Qayta urinib ko'rish"
                        LessonFragment.vp.setAllowedSwipeDirection(SwipeDirection.LEFT)
                    }
                }
                1 -> {
                    LessonFragment.vp.currentItem += 1
                }
                3 -> {
                    root.rg.clearCheck()
                    for (i in 0 until 4) {
                        root.rg.getChildAt(i).isEnabled = true
                    }
                    root.btn_check.text = "Tekshirish"
                    isCheck = 0
                }
                5 -> {
                    updateFinish(currentUser)
                    startActivity(
                        Intent(
                            requireContext(),
                            MainActivity::class.java
                        )
                    )
                }
            }
        }
        return root
    }

    fun updateUserScore(currentUser: FirebaseUser, listSize: Int) {
        var user: Map<String, Any> = HashMap()
        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(currentUser.uid)
            .get()
            .addOnSuccessListener { it ->
                user = it.data!!
                val userRef =
                    FirebaseFirestore.getInstance().collection("users")
                        .document(currentUser.uid)
                userRef.update(
                    "score",
                    user["score"].toString().toInt() + listSize
                )
                Log.d(ContentValues.TAG, "updateUsers: ${it.data}")
            }.addOnFailureListener {
                Log.d(ContentValues.TAG, "getUserList: $it")
            }
    }

    fun updateFinish(currentUser: FirebaseUser) {
        var user: Map<String, Any> = HashMap()
        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(currentUser.uid)
            .get()
            .addOnSuccessListener { it ->
                user = it.data!!
                val userRef =
                    FirebaseFirestore.getInstance().collection("users")
                        .document(currentUser.uid)
                userRef.update(
                    "haveFinished",
                    true
                )
                Log.d(ContentValues.TAG, "updateUsers: ${it.data}")
            }.addOnFailureListener {
                Log.d(ContentValues.TAG, "getUserList: $it")
            }
    }

    fun nextPlanData(currentUser: FirebaseUser) {
        var titleId = basicPlan?.id.toString()
        val docRef =
            FirebaseFirestore.getInstance().collection("Basic Plans")
                .document(titleId)
        docRef.update(
            "haveUsedUsers",
            FieldValue.arrayUnion(currentUser.uid)
        )
        val docRef1 =
            FirebaseFirestore.getInstance().collection("Basic Plans")
                .document((titleId.toInt() + 1).toString())
        docRef1.update(
            "areUsingUsers",
            FieldValue.arrayUnion(currentUser.uid)
        )
    }

    companion object {
        @JvmStatic
        fun newInstance(
            test: Test,
            listSize: Int,
            plan: Plan,
            isEnd: Boolean,
            basicPlan: BasicPlan
        ) =
            TestFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM1, test)
                    putInt(ARG_PARAM2, listSize)
                    putSerializable(ARG_PARAM3, plan)
                    putBoolean(ISEND, isEnd)
                    putSerializable(BASICPLAN,basicPlan)
                }
            }
    }
}