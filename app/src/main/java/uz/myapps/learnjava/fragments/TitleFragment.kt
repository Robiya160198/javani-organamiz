package uz.myapps.learnjava.fragments

import android.content.ContentValues
import android.content.Intent
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_title.view.*
import uz.myapps.learnjava.CoursesActivity
import uz.myapps.learnjava.MainActivity
import uz.myapps.learnjava.R
import uz.myapps.learnjava.viewmodels.JavaViewModel


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class TitleFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_title, container, false)
        MainActivity.bottom_view.visibility = View.VISIBLE
        val imgs = loadImgs()
        val currentUser = FirebaseAuth.getInstance().currentUser!!
        val javaViewModel = ViewModelProviders.of(this)[JavaViewModel::class.java]
        javaViewModel.getBasicPlanData().observe(requireActivity(), Observer {
            root.pbar.visibility=View.GONE
            root.scrollView.visibility=View.VISIBLE
            val intent = Intent(requireContext(), CoursesActivity::class.java)
            root.title1.text = it[0].name
            root.title2.text = it[1].name
            root.title3.text = it[2].name
            root.title4.text = it[3].name
            root.title5.text = it[4].name
            root.title6.text = it[5].name
            root.title7.text = "Sertifikat"
            root.img1.setImageResource(imgs[0])
            root.img2.setImageResource(imgs[1])
            root.img3.setImageResource(imgs[2])
            root.img4.setImageResource(imgs[3])
            root.img5.setImageResource(imgs[4])
            root.img6.setImageResource(imgs[5])
            root.img7.setImageResource(imgs[6])

            var user: Map<String, Any> = HashMap()
            val db = FirebaseFirestore.getInstance()
            db.collection("users").document(currentUser.uid)
                .get()
                .addOnSuccessListener { it ->
                    user = it.data!!
                  if(user["haveFinished"] as Boolean){
                      root.img7.isClickable = true
                      root.img7.setOnClickListener { view ->
                          fragmentManager?.beginTransaction()?.replace(R.id.fragment_container,CertificateFragment())?.commit()
                      }
                  }
                    else{
                      val matrix = ColorMatrix()
                      matrix.setSaturation(0f)
                      root.img7.colorFilter = ColorMatrixColorFilter(matrix)
                  }
                    Log.d(ContentValues.TAG, "updateUsers: ${it.data}")
                }.addOnFailureListener {
                    Log.d(ContentValues.TAG, "getUserList: $it")
                }

            for (i in it) {
                if (i.areUsingUsers.contains(currentUser.uid) || i.haveUsedUsers.contains(
                        currentUser.uid
                    )
                ) {
                    when (i.id) {
                        "2" -> {
                            root.img2.setOnClickListener { view ->
                                intent.putExtra("basicPlan", i)
                                startActivity(intent)
                                requireActivity()!!.overridePendingTransition(
                                    R.anim.slide_in_right,
                                    R.anim.slide_out_left
                                )
                            }
                        }
                        "3" -> {
                            root.img3.setOnClickListener { view ->
                                intent.putExtra("basicPlan", i)
                                startActivity(intent)
                                requireActivity()!!.overridePendingTransition(
                                    R.anim.slide_in_right,
                                    R.anim.slide_out_left
                                )
                            }
                        }
                        "4" -> {
                            root.img4.setOnClickListener { view ->
                                intent.putExtra("basicPlan", i)
                                startActivity(intent)
                                requireActivity()!!.overridePendingTransition(
                                    R.anim.slide_in_right,
                                    R.anim.slide_out_left
                                )
                            }
                        }
                        "5" -> {
                            root.img5.setOnClickListener { view ->
                                intent.putExtra("basicPlan", i)
                                startActivity(intent)
                                requireActivity()!!.overridePendingTransition(
                                    R.anim.slide_in_right,
                                    R.anim.slide_out_left
                                )
                            }
                        }
                        "6" -> {
                            root.img6.setOnClickListener { view ->
                                intent.putExtra("basicPlan", i)
                                startActivity(intent)
                                requireActivity()!!.overridePendingTransition(
                                    R.anim.slide_in_right,
                                    R.anim.slide_out_left
                                )
                            }
                        }
                    }
                } else {
                    when (i.id) {
                        "2" -> {
                            val matrix = ColorMatrix()
                            matrix.setSaturation(0f)
                            root.img2.colorFilter = ColorMatrixColorFilter(matrix)
                        }
                        "3" -> {
                            val matrix = ColorMatrix()
                            matrix.setSaturation(0f)
                            root.img3.colorFilter = ColorMatrixColorFilter(matrix)
                        }
                        "4" -> {
                            val matrix = ColorMatrix()
                            matrix.setSaturation(0f)
                            root.img4.colorFilter = ColorMatrixColorFilter(matrix)
                        }
                        "5" -> {
                            val matrix = ColorMatrix()
                            matrix.setSaturation(0f)
                            root.img5.colorFilter = ColorMatrixColorFilter(matrix)
                        }
                        "6" -> {
                            val matrix = ColorMatrix()
                            matrix.setSaturation(0f)
                            root.img6.colorFilter = ColorMatrixColorFilter(matrix)
                        }
                    }
                }
            }
            root.img1.setOnClickListener { view ->
                intent.putExtra("basicPlan", it[0])
                startActivity(intent)
                requireActivity()!!.overridePendingTransition(
                    R.anim.slide_in_right,
                    R.anim.slide_out_left
                )
            }
        })
        return root
    }

    private fun loadImgs(): ArrayList<Int> {
        var list = ArrayList<Int>()
        list.add(R.drawable.ic_introduction)
        list.add(R.drawable.loop3)
        list.add(R.drawable.shapes)
        list.add(R.drawable.objectsandclasses)
        list.add(R.drawable.polimorfizm)
        list.add(R.drawable.list)
        list.add(R.drawable.certificate)
        return list
    }

}