package uz.myapps.learnjava.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ShareCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import uz.myapps.learnjava.CoursesActivity
import uz.myapps.learnjava.MainActivity
import uz.myapps.learnjava.R
import uz.myapps.learnjava.SplashActivity
import uz.myapps.learnjava.databinding.FragmentStartBinding
import uz.myapps.learnjava.viewmodels.JavaViewModel

class ProfileFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_profile, container, false)
        MainActivity.bottom_view.visibility=View.VISIBLE
        val instance = FirebaseAuth.getInstance()
        val javaViewModel = ViewModelProviders.of(this)[JavaViewModel::class.java]
        javaViewModel.getUserData(instance.currentUser!!).observe(requireActivity(), Observer {
            inflate.name_profile.text = it.name
            inflate.email_profile.text = it.email
            inflate.score.text = it.score.toString()
            inflate.ball.text = it.score.toString()
            Picasso.get().load(it.img).into(inflate.img_profile)
        })
        inflate.profile_settings.setOnClickListener {
            fragmentManager?.beginTransaction()?.replace(R.id.fragment_container,EditProfileFragment())?.addToBackStack("Profile")?.commit()
        }
        inflate.btn_settings.setOnClickListener {
            fragmentManager?.beginTransaction()?.replace(R.id.fragment_container,SettingsFragment())?.addToBackStack("Settings")?.commit()
        }
        inflate.btn_share.setOnClickListener {
            ShareCompat.IntentBuilder.from(requireActivity())
                .setType("text/plain")
                .setChooserTitle("Chooser title")
                .setText("Tez kunda ko'rishguncha")
                .startChooser()
        }
        inflate.sendIdea.setOnClickListener {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "farmonovarobiya@gmail.com", null
                )
            )
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Fikringiz mavzusi")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "fikringiz..")
            startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }
        inflate.btn_rate.setOnClickListener {
            var gourl = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=uz.myapps.learnjava")
            )
            startActivity(gourl)
        }
        inflate.logout.setOnClickListener {
            val sharedPreferences =
                requireContext().getSharedPreferences("DarkMode", Context.MODE_PRIVATE)
            val edit = sharedPreferences.edit()
            edit.putInt("isDark", 0)
            edit.apply()
            edit.commit()
            instance.signOut()
            startActivity(Intent(requireContext(), SplashActivity::class.java))
            requireActivity().finish()
        }
        return inflate
    }
}