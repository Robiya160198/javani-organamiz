package uz.myapps.learnjava.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_lesson.view.*
import uz.myapps.learnjava.customs.CustomViewPager
import uz.myapps.learnjava.R
import uz.myapps.learnjava.adapter.LessonPageViewAdapter
import uz.myapps.learnjava.adapter.QuizAdapter
import uz.myapps.learnjava.models.BasicPlan
import uz.myapps.learnjava.models.Lesson
import uz.myapps.learnjava.models.Plan
import uz.myapps.learnjava.models.Test
import uz.myapps.learnjava.viewmodels.JavaViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class LessonFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_lesson, container, false)
        var plan: Plan = Plan()
        if(arguments?.getSerializable("plan")!="")
        plan = arguments?.getSerializable("plan")!! as Plan
        var basicPlan = arguments?.getSerializable("basicPlan")!! as BasicPlan
        val isEnd = arguments?.getBoolean("isEnd")!!
        var testList = ArrayList<Test>()
        vp = root.vp
        val javaViewModel = ViewModelProviders.of(this)[JavaViewModel::class.java]
        if(plan.id==0){
            var adapter =
                QuizAdapter(
                    testList,
                    plan,
                    basicPlan,
                    requireActivity().supportFragmentManager
                )
            vp.adapter = adapter
            javaViewModel.getTestData().observe(requireActivity(), Observer {
                for (test in it) {
                    val id = test.titleId
                    if (test.id == basicPlan.id&&id=="0") {
                        testList.add(test)
                    }
                }
                adapter.notifyDataSetChanged()
            })
        }else {
            var lectureList = ArrayList<Lesson>()

            var adapter =
                LessonPageViewAdapter(
                    lectureList,
                    testList,
                    plan,
                    isEnd,
                    basicPlan,
                    requireActivity().supportFragmentManager
                )
            vp.adapter = adapter

            javaViewModel.getLectureData().observe(requireActivity(), Observer {
                for (lesson in it) {
                    var id = lesson.titleId.substring(0, lesson.titleId.length - 2)
                    if (id == plan.id.toString()) {
                        lectureList.add(lesson)
                    }
                }
                //   adapter.notifyDataSetChanged()
            })
            javaViewModel.getTestData().observe(requireActivity(), Observer {
                for (test in it) {
                    val id = test.titleId
                    if (id == plan.id.toString()) {
                        testList.add(test)
                    }
                }
                adapter.notifyDataSetChanged()
            })
        }
        root.back_lecture.setOnClickListener {
            requireActivity().onBackPressed()
        }
//        root.btn_text_size.setOnClickListener {
//            LectureFragment.lectureName.textSize = 35f
//            LectureFragment.part1.textSize = 25f
//            LectureFragment.part2.textSize = 25f
//            LectureFragment.code1.textSize = 25f
//            LectureFragment.code2.textSize = 25f
//            LectureFragment.note.textSize = 25f
//        }
        return root
    }

    companion object {
        lateinit var vp: CustomViewPager

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            LessonFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}