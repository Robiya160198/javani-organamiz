package uz.myapps.learnjava.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_settings.view.*
import uz.myapps.learnjava.MainActivity
import uz.myapps.learnjava.R

class SettingsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_settings, container, false)
        val sharedPreferences =
            requireContext().getSharedPreferences("DarkMode", Context.MODE_PRIVATE)

        root.darkMode.isChecked = sharedPreferences.getInt("isDark",0)==1

        val edit = sharedPreferences.edit()
        root.rlprofile.setOnClickListener {
            fragmentManager?.beginTransaction()
                ?.replace(R.id.fragment_container, EditProfileFragment())
                ?.addToBackStack("EditProfile")?.commit()
        }
        root.rltextsize.setOnClickListener {
            fragmentManager?.beginTransaction()
                ?.replace(R.id.fragment_container, TextSizeFragment())
                ?.addToBackStack("EditProfile")?.commit()
        }
        root.darkMode.setOnCheckedChangeListener { _, b ->
            if (b) {
                edit.putInt("isDark", 1)
                edit.apply()
                edit.commit()
                    startActivity(Intent(requireContext(),MainActivity::class.java))
                    MainActivity().finish()
            }
            else{
                edit.putInt("isDark", 0)
                edit.apply()
                edit.commit()
                    startActivity(Intent(requireContext(),MainActivity::class.java))
                    MainActivity().finish()
            }
        }
        return root
    }

}