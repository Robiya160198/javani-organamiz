package uz.myapps.learnjava.fragments

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_edit_info.view.*
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import kotlinx.android.synthetic.main.fragment_edit_profile.view.*
import kotlinx.android.synthetic.main.fragment_edit_profile.view.name
import uz.myapps.learnjava.CoursesActivity
import uz.myapps.learnjava.MainActivity
import uz.myapps.learnjava.R
import uz.myapps.learnjava.SplashActivity
import uz.myapps.learnjava.viewmodels.JavaViewModel
import java.io.ByteArrayOutputStream
import java.io.File
import java.lang.Exception

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [EditProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EditProfileFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var byteArray: ByteArray
    lateinit var instance: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_edit_profile, container, false)
        MainActivity.bottom_view.visibility = View.GONE
        val javaViewModel = ViewModelProviders.of(this)[JavaViewModel::class.java]
        instance = FirebaseAuth.getInstance()
        javaViewModel.getUserData(instance.currentUser!!).observe(requireActivity(), Observer {
            root.name.text = it.name
            root.email.text = it.email
            root.password.text = it.password
            Picasso.get().load(it.img).into(root.img_profile_edit)
        })
        root.rl2.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("name", root.name.text.toString())
            bundle.putString("type", "Ism")
            val editInfoFragment = EditInfoFragment()
            editInfoFragment.arguments = bundle
            fragmentManager?.beginTransaction()?.replace(R.id.fragment_container, editInfoFragment)
                ?.addToBackStack("EditProfile")?.commit()
        }

        root.rl4.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("name", root.password.text.toString())
            bundle.putString("type", "Parol")
            val editInfoFragment = EditInfoFragment()
            editInfoFragment.arguments = bundle
            fragmentManager?.beginTransaction()?.replace(R.id.fragment_container, editInfoFragment)
                ?.addToBackStack("EditProfile")?.commit()
        }
        root.edit_img.setOnClickListener {
            checkPermission()
            var intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(intent, 0)
        }
        root.back_edit_profile.setOnClickListener {
            requireActivity().onBackPressed()
        }
        root.btn_logout.setOnClickListener {
            val sharedPreferences =
                requireContext().getSharedPreferences("DarkMode", Context.MODE_PRIVATE)
            val edit = sharedPreferences.edit()
            edit.putInt("isDark", 0)
            edit.apply()
            edit.commit()
            instance.signOut()
            startActivity(Intent(requireContext(), SplashActivity::class.java))
            requireActivity().finish()
        }
        return root
    }

    private fun checkPermission() {
        ContextCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
        ContextCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
        ContextCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            1
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                0 -> {
                    val uri = data?.data
                    var array = arrayOf(MediaStore.Images.Media.DATA)
                    if (uri != null) {
                        var cursor =
                            requireContext().contentResolver.query(uri, array, null, null, null)
                        cursor?.moveToFirst()
                        val columnIndex = cursor?.getColumnIndex(array[0])
                        var picturePath = cursor?.getString(columnIndex!!)
                        val bitmap = BitmapFactory.decodeFile(picturePath)
                        var byteArrayOutputStream = ByteArrayOutputStream()
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
                        byteArray = byteArrayOutputStream.toByteArray()
                        img_profile_edit.setImageBitmap(bitmap)
                        updateImg(byteArray)
                        cursor?.close()
                    }
                }
            }
        }
    }

    private fun updateImg(byteArray: ByteArray) {
        val reference = FirebaseStorage.getInstance().reference.child(instance.currentUser!!.uid)
        reference.putBytes(byteArray).addOnSuccessListener {
            reference.downloadUrl.addOnSuccessListener {
                val userRef =
                    FirebaseFirestore.getInstance().collection("users")
                        .document(instance.currentUser!!.uid)
                userRef.update(
                    "img",
                    it.toString()
                )
            }

        }

    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            EditProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}