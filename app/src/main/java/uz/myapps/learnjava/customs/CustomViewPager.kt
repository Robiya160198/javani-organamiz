package uz.myapps.learnjava.customs

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager
import uz.myapps.learnjava.enums.SwipeDirection

class CustomViewPager : ViewPager {
    private var disable = false
    private var initialXValue = 0f
    private var direction: SwipeDirection? = null
    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!,
        attrs
    ) {
        this.direction = SwipeDirection.ALL;
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        if (this.IsSwipeAllowed(event)) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (this.IsSwipeAllowed(event)) {
            return super.onTouchEvent(event);
        }
        return false;
    }

    private fun IsSwipeAllowed(event: MotionEvent): Boolean {
        if(this.direction == SwipeDirection.ALL) return true;

        if(direction == SwipeDirection.NONE )//disable any swipe
            return false;

        if(event.action ==MotionEvent.ACTION_DOWN) {
            initialXValue = event.getX();
            return true;
        }

        if (event.action === MotionEvent.ACTION_MOVE) {
            val diffX = event.x - initialXValue
            if (diffX > 0 && direction === SwipeDirection.RIGHT) {
                // swipe from left to right detected
                return false
            } else if (diffX < 0 && direction === SwipeDirection.LEFT) {
                // swipe from right to left detected
                return false
            }
        }

        return true;
    }

    fun setAllowedSwipeDirection(direction: SwipeDirection?) {
        this.direction = direction
    }

    fun disableScroll(disable: Boolean) {
        //When disable = true not work the scroll and when disble = false work the scroll
        this.disable = disable
    }
}