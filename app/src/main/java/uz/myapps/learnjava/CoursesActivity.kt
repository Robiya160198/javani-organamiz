package uz.myapps.learnjava

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import kotlinx.android.synthetic.main.activity_courses.*
import uz.myapps.learnjava.fragments.MenuFragment

class CoursesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val sharedPreferences =
            this.getSharedPreferences("DarkMode", Context.MODE_PRIVATE)
        when(sharedPreferences.getInt("size", 1)){
            0 ->setTheme(R.style.MyTheme)
            1 ->setTheme(R.style.MyThemeMiddleTextSize)
            2 ->setTheme(R.style.MyThemeLargeTextSize)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_courses)
        if (sharedPreferences.getInt("isDark", 0) == 1) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            container.setBackgroundColor(Color.parseColor("#222222"))
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            container.setBackgroundColor(Color.parseColor("#eeeeee"))
        }
        val basicPlan = intent.getSerializableExtra("basicPlan")
        val menuFragment = MenuFragment()
        val bundle = Bundle()
        bundle.putSerializable("basicPlan",basicPlan)
        menuFragment.arguments=bundle
        supportFragmentManager.beginTransaction().add(R.id.container,menuFragment).commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right)
    }
}