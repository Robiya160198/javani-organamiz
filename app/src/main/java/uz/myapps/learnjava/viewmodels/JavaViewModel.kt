package uz.myapps.learnjava.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseUser
import uz.myapps.learnjava.models.*
import uz.myapps.learnjava.repository.JavaRepository

class JavaViewModel:ViewModel() {
    var repository= JavaRepository()
    fun getPlanDataByTitleId(basicPlanId: String):MutableLiveData<List<Plan>>{
        return repository.getPlanListByTitleId(basicPlanId)
    }
    fun getUsers():MutableLiveData<List<User>>{
        return repository.getUsers()
    }
    fun getBasicPlanData():MutableLiveData<List<BasicPlan>>{
        return repository.getBasicPlanList()
    }
    fun getLectureData():MutableLiveData<List<Lesson>>{
        return repository.getLessonList()
    }
    fun getTestData():MutableLiveData<List<Test>>{
        return repository.getTestList()
    }
    fun updateUsers(
        titleId: String,
        id: String,
        currentUser: FirebaseUser,
        isEnd: Boolean
    ):MutableLiveData<List<User>>{
        return repository.updateUsers(titleId,id,currentUser,isEnd)
    }
    fun getUserData(currentUser: FirebaseUser):MutableLiveData<User>{
        return repository.getUser(currentUser)
    }
}