package uz.myapps.learnjava.handler

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import uz.myapps.learnjava.R


class MyHandler {
    fun goFragment(view: View, fragment: Fragment) {
        if (view.context is AppCompatActivity) {
            (view.context as AppCompatActivity).supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right,R.anim.slide_out_left,R.anim.slide_in_left,R.anim.slide_out_right).addToBackStack("main")
                .replace(R.id.fragment, fragment).commit()
        }
    }
}