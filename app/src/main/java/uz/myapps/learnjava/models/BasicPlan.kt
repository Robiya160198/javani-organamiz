package uz.myapps.learnjava.models

import java.io.Serializable

class BasicPlan(
    var id: String = "",
    var name: String = "",
    var areUsingUsers:List<String> = ArrayList(),
    var haveUsedUsers:List<String> = ArrayList()
):Serializable