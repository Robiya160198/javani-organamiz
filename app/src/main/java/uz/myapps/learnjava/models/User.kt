package uz.myapps.learnjava.models

import java.io.Serializable

class User : Serializable {
    var email: String? = ""
    var name: String? = ""
    var password: String? = ""
    var score: Int? = 0
    var img: String? = ""
    var haveFinished:Boolean = false

    constructor()
    constructor(email: String, name: String, password: String, score: Int, img: String) {
        this.email = email
        this.name = name
        this.password = password
        this.score = score
        this.img = img
    }

    constructor(
        email: String?,
        name: String?,
        password: String?,
        score: Int?,
        img: String?,
        haveFinished: Boolean
    ) {
        this.email = email
        this.name = name
        this.password = password
        this.score = score
        this.img = img
        this.haveFinished = haveFinished
    }

}