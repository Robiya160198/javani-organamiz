package uz.myapps.learnjava.models

import java.io.Serializable

class Test (
    var question:String = "",
    var answer1:String = "",
    var answer2:String = "",
    var answer3:String = "",
    var answer4:String = "",
    var realAnswer:String = "",
    var titleId:String = "",
    var id:String = ""
):Serializable