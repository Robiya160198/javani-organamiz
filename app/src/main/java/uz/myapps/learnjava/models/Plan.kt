package uz.myapps.learnjava.models

import java.io.Serializable

class Plan: Serializable {
    var name: String = ""
    var titleId: String = ""
    var id: Int = 0
    var areUsingUsers: List<String> = ArrayList()
    var haveUsedUsers: List<String> = ArrayList()

    constructor()
}

