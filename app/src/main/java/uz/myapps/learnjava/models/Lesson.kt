package uz.myapps.learnjava.models

import java.io.Serializable

class Lesson(
    var name: String = "",
    var part1: String = "",
    var code1: String = "",
    var part2: String = "",
    var code2: String = "",
    var note: String = "",
    var titleId:String =""
) : Serializable